# Toy Robot Challenge
by Richard Lee

### Requirements
```sh
> NodeJS
> NPM
```

### Installation
Merely, install the dependencies.
```sh
$ npm i
```

### To run the app
```sh
$ npm run start
```

### To run the tests
```sh
$ npm run test
```

### To update the robot commands
```sh
> Find the input.json file in project root
> Update the "commands" array
```

### Tech used

| Plugin |
| ------ |
| NodeJS |
| Typescript |
| JS (ES6) |
| Jest |
