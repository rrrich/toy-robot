import { InputValidator } from "./models/InputValidator";
import { Session } from "./models/Session";

// tslint:disable-next-line:no-var-requires
const input: { commands: string[] } = require("./input.json");

(function main(sessionInput) {

    const session = new Session();
    const inputValidator = new InputValidator();
    const commands = sessionInput.commands;

    commands.forEach((command: string) => {

        const commandPrefix = command.split(" ");
        if (!inputValidator.isValidCommand(commandPrefix[0])) {
            return;
        }

        switch (commandPrefix[0]) {
            case "PLACE":
                const placePieces: string[] = (command.split(" ") as string[])[1].split(",");
                const xPos: number = +placePieces[0];
                const yPos: number = +placePieces[1];
                const direction: string = placePieces[2];
                session.moveRobotTo(xPos, yPos, direction, true);
                break;
            case "MOVE":
                session.shiftRobot();
                break;
            case "LEFT":
            case "RIGHT":
                session.rotateRobot(commandPrefix[0]);
                break;
            case "REPORT":
                console.log(session.robotStatusDescription());
                break;
            default:
                break;
        }

    });

}(input));
