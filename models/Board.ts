export class Board {
    heightUnits: number;
    widthUnits: number;

    constructor(widthUnits: number, heightUnits: number) {
        this.widthUnits = widthUnits;
        this.heightUnits = heightUnits;
    }

    maxXBounds(): number {
        return this.widthUnits - 1;
    }

    maxYBounds(): number {
        return this.heightUnits - 1;
    }

    minXBounds(): number {
        return 0;
    }

    minYBounds(): number {
        return 0;
    }

    isMoveInBounds(xPos: number, yPos: number): boolean {
        return ((xPos >= this.minXBounds() && xPos <= this.maxXBounds()) && (yPos >= this.minYBounds() && yPos <= this.maxYBounds()));
    }
}
