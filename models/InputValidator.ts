export class InputValidator {
    private readonly VALID_INPUT_PREFIXES = [
        "PLACE",
        "MOVE",
        "LEFT",
        "RIGHT",
        "REPORT",
    ];

    isValidCommand(command: string): boolean {
        return this.VALID_INPUT_PREFIXES.includes(command);
    }
}
