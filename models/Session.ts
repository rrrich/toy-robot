import { Board } from "./Board";
import { DirectionLookup } from "./DirectionLookup";
import { Move } from "./Move";
import { MoveGenerator } from "./MoveGenerator";
import { ToyRobot } from "./ToyRobot";

export class Session {

    private hasRobotBeenPlaced = false;
    private board: Board;
    private moveGenerator: MoveGenerator;
    private directionLookup: DirectionLookup;
    private toyRobot: ToyRobot;

    constructor() {
        this.toyRobot = new ToyRobot();
        this.board = new Board(5, 5);
        this.moveGenerator = new MoveGenerator();
        this.directionLookup = new DirectionLookup();
    }

    robotStatusDescription(): string {
        const directionDescription = this.directionLookup.descriptionForDegrees(this.toyRobot.rotationDegrees);
        return `${this.toyRobot.report()},${directionDescription}`;
    }

    rotateRobot(rotationDescription: string): void {
        if (!this.hasRobotBeenPlaced) {
            return;
        }

        const rotationDegrees = this.directionLookup.rotationDegreesLookup[rotationDescription];
        this.toyRobot.rotate(rotationDegrees);
    }

    shiftRobot(): void {
        const move = this.moveGenerator.generateShiftMove({ x: this.toyRobot.xPos, y: this.toyRobot.yPos }, 1, this.toyRobot.rotationDegrees);
        this.conductMove(move);
    }

    moveRobotTo(x: number, y: number, directionDescription: string, isPlaceMove?: boolean): void {
        const degrees = this.directionLookup.directionDegreesLookup[directionDescription];

        if (typeof degrees === "number") {
            const move = this.moveGenerator.generateMove(x, y, degrees);
            this.conductMove(move, isPlaceMove || undefined);
        }
    }

    private conductMove(move: Move, isPlaceMove?: boolean): void {

        if (!this.canConductMove(move, isPlaceMove || undefined)) {
            return;
        }

        this.toyRobot.move(move.xPos, move.yPos, move.directionDegrees);
        if (isPlaceMove !== undefined && !!isPlaceMove) {
            this.hasRobotBeenPlaced = isPlaceMove;
        }
    }

    private canConductMove(move: Move, isPlaceMove?: boolean): boolean {
        return (this.hasRobotBeenPlaced || isPlaceMove) && this.board.isMoveInBounds(move.xPos, move.yPos);
    }
}
