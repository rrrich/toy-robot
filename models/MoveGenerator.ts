import { Move } from "./Move";

export class MoveGenerator {

    generateShiftMove(currPosition: { x: number, y: number }, units: number, directionDegrees: number): Move {
        const move = new Move();
        move.xPos = currPosition.x;
        move.yPos = currPosition.y;
        switch (directionDegrees) {
            case 0:
                move.yPos += units;
                break;
            case 90:
                move.xPos += units;
                break;
            case 180:
                move.yPos -= units;
                break;
            case 270:
                move.xPos -= units;
                break;
            default:
                break;
        }
        return move;
    }

    generateMove(x: number, y: number, directionDegrees: number): Move {
        const m = new Move();
        m.xPos = x;
        m.yPos = y;
        m.directionDegrees = directionDegrees;
        return m;
    }
}
