export class Move {
    public yPos: number;
    public xPos: number;
    public directionDegrees: number;
}
