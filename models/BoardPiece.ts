export default class BoardPiece {
    xPos = 0;
    yPos = 0;

    report(): string {
        return `${this.xPos},${this.yPos}`;
    }
}
