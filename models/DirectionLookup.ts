import { IDictionary } from "./IDictionary";

export class DirectionLookup {

    rotationDegreesLookup: IDictionary<number> = {
        LEFT: -90,
        RIGHT: 90,
    };

    directionDegreesLookup: IDictionary<number> = {
        EAST: 90,
        NORTH: 0,
        SOUTH: 180,
        WEST: 270,
    };

    descriptionForDegrees(degrees: number): string {
        const keys = Object.keys(this.directionDegreesLookup);
        return keys.find((k: string) => this.directionDegreesLookup[k] === degrees);
    }
}
