import BoardPiece from "./BoardPiece";
import { RotationType } from "./RotationType.enum";

export class ToyRobot extends BoardPiece {

    rotationDegrees: number;

    private VALID_ROTATION_INPUTS = [
        90, -90,
    ];

    private ROTATION_STATES = [
        0,
        90,
        180,
        270,
    ];

    constructor() {
        super();
        this.rotationDegrees = 0;
    }

    move(xPos: number, yPos: number, rotationDegrees: number = -1): void {
        this.xPos = xPos;
        this.yPos = yPos;
        if (rotationDegrees !== -1) {
            this.rotationDegrees = rotationDegrees;
        }
    }

    rotate(rotationAmount: RotationType): void {

        if (!rotationAmount || !this.VALID_ROTATION_INPUTS.includes(rotationAmount)) {
            return;
        }

        const currRotationIdx = this.ROTATION_STATES.findIndex((rs: number) => rs === this.rotationDegrees);

        if (currRotationIdx !== -1) {
            let newIdx: number;
            if (rotationAmount === RotationType.RIGHT) {
                newIdx = (currRotationIdx + 1) >= this.ROTATION_STATES.length ? 0 : (currRotationIdx + 1);
            } else {
                newIdx = (currRotationIdx - 1) < 0 ? (this.ROTATION_STATES.length - 1) : (currRotationIdx - 1);
            }
            this.rotationDegrees = this.ROTATION_STATES[newIdx];
        }
    }

}
