import { Session } from "../models/Session";

describe("robotStatusDescription", () => {

    test("should report 0,0,NORTH", () => {
        const session = new Session();
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });
});

describe("shiftRobot", () => {

    test("should leave robot position unchanged before place move", () => {
        const session = new Session();
        session.shiftRobot();
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });

    test("should decrement xpos when facing WEST", () => {
        const session = new Session();
        session.moveRobotTo(3, 3, "WEST", true);
        session.shiftRobot();
        expect(session.robotStatusDescription()).toBe("2,3,WEST");
    });

    test("should increment xpos when facing EAST", () => {
        const session = new Session();
        session.moveRobotTo(3, 3, "EAST", true);
        session.shiftRobot();
        expect(session.robotStatusDescription()).toBe("4,3,EAST");
    });

    test("should increment ypos when facing NORTH", () => {
        const session = new Session();
        session.moveRobotTo(0, 0, "NORTH", true);
        session.shiftRobot();
        expect(session.robotStatusDescription()).toBe("0,1,NORTH");
    });

    test("should decrement ypos when facing SOUTH", () => {
        const session = new Session();
        session.moveRobotTo(3, 3, "SOUTH", true);
        session.shiftRobot();
        expect(session.robotStatusDescription()).toBe("3,2,SOUTH");
    });
});

describe("moveRobotTo", () => {

    test("should place robot at correct position", () => {
        const session = new Session();
        session.moveRobotTo(4, 4, "WEST", true);
        expect(session.robotStatusDescription()).toBe("4,4,WEST");
    });

    test("should leave robot position unchanged when coords are out of bounds", () => {
        const session = new Session();
        session.moveRobotTo(5, 5, "WEST", true);
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });

    test("should leave robot position unchanged when coords [-1,3]", () => {
        const session = new Session();
        session.moveRobotTo(-1, 3, "WEST", true);
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });

    test("should leave robot position unchanged when coords [2,-1]", () => {
        const session = new Session();
        session.moveRobotTo(2, -1, "WEST", true);
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });

    test("should leave robot position unchanged before place move", () => {
        const session = new Session();
        session.moveRobotTo(4, 4, "WEST", false);
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });

    test("should leave robot position unchanged before place move - place argument removed", () => {
        const session = new Session();
        session.moveRobotTo(4, 4, "WEST");
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });

    test("should leave robot position unchanged if direction description invalid", () => {
        const session = new Session();
        session.moveRobotTo(4, 4, "FOO", false);
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });
});

describe("rotateRobot", () => {

    test("should leave robot position unchanged before place move", () => {
        const session = new Session();
        session.rotateRobot("LEFT");
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });

    test("should leave robot position unchanged before place move", () => {
        const session = new Session();
        session.rotateRobot("LEFT");
        expect(session.robotStatusDescription()).toBe("0,0,NORTH");
    });
});
