import { ToyRobot } from "../models/ToyRobot";

describe("rotate", () => {

    test("should set rotationDegrees to 90 after providing 90", () => {
        const tr = new ToyRobot();
        tr.rotate(90);
        expect(tr.rotationDegrees).toBe(90);
    });

    test("should set rotationDegrees to 270 after providing -90", () => {
        const tr = new ToyRobot();
        tr.rotate(-90);
        expect(tr.rotationDegrees).toBe(270);
    });

    test("should set rotationDegrees to 180 after providing -90 again", () => {
        const tr = new ToyRobot();
        tr.rotate(-90);
        tr.rotate(-90);
        expect(tr.rotationDegrees).toBe(180);
    });

    test("should set rotationDegrees to 0 after providing -90", () => {
        const tr = new ToyRobot();
        tr.rotate(90);
        tr.rotate(-90);
        expect(tr.rotationDegrees).toBe(0);
    });

    test("should leave rotationDegrees as 0 after providing illegal rotation amount", () => {
        const tr = new ToyRobot();
        tr.rotate(1);
        expect(tr.rotationDegrees).toBe(0);
    });

    test("should not change rotationDegrees when provided undefined", () => {
        const tr = new ToyRobot();
        tr.rotationDegrees = 90;
        tr.rotate(undefined);
        expect(tr.rotationDegrees).toBe(90);
    });
});

describe("move", () => {

    test("should set yPos to 1 after providing [0, 1]", () => {
        const tr = new ToyRobot();
        const mockMove = { x: 0, y: 1 };
        tr.move(mockMove.x, mockMove.y);
        expect(tr.yPos).toBe(1);
    });

    test("should set xPos to 0 after providing [0, 1]", () => {
        const tr = new ToyRobot();
        const mockMove = { x: 0, y: 1 };
        tr.move(mockMove.x, mockMove.y);
        expect(tr.xPos).toBe(0);
    });

    test("should set leave rotationDegrees unchanged when not providing argument", () => {
        const tr = new ToyRobot();
        tr.rotationDegrees = 90;

        const mockMove = { x: 0, y: 1 };
        tr.move(mockMove.x, mockMove.y);
        expect(tr.rotationDegrees).toBe(90);
    });

    test("should set leave rotationDegrees unchanged when not provided undefined", () => {
        const tr = new ToyRobot();
        tr.rotationDegrees = 90;

        const mockMove = { x: 0, y: 1 };
        tr.move(mockMove.x, mockMove.y, undefined);
        expect(tr.rotationDegrees).toBe(90);
    });

    test("should set rotationDegrees to 0 when provided 0", () => {

        const tr = new ToyRobot();
        tr.rotationDegrees = 10;
        tr.rotationDegrees = 20;

        const mockMove = { x: 0, y: 1 };
        tr.move(mockMove.x, mockMove.y, 0);
        expect(tr.rotationDegrees).toBe(0);
    });

    test("should set rotationDegrees to 180 when provided 180 as argument", () => {
        const tr = new ToyRobot();
        tr.rotationDegrees = 90;

        const mockMove = { x: 0, y: 1 };
        tr.move(mockMove.x, mockMove.y, 180);
        expect(tr.rotationDegrees).toBe(180);
    });
});
