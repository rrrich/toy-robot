import { Board } from "../models/Board";

describe("isMoveInBounds", () => {
    const board = new Board(5, 5);

    test("should return true when provided [0, 0] as coords", () => {
        const mockMove = { x: 0, y: 0 };
        expect(board.isMoveInBounds(mockMove.x, mockMove.y)).toBe(true);
    });

    test("should return true when provided [4, 4] as coords", () => {
        const mockMove = { x: 4, y: 4 };
        expect(board.isMoveInBounds(mockMove.x, mockMove.y)).toBe(true);
    });

    test("should return false when provided [5, 5] as coords", () => {
        const mockMove = { x: 5, y: 5 };
        expect(board.isMoveInBounds(mockMove.x, mockMove.y)).toBe(false);
    });

    test("should return false when provided [0, -1] as coords", () => {
        const mockMove = { x: 0, y: -1 };
        expect(board.isMoveInBounds(mockMove.x, mockMove.y)).toBe(false);
    });

    test("should return false when provided [10, 10] as coords", () => {
        const mockMove = { x: 10, y: 10 };
        expect(board.isMoveInBounds(mockMove.x, mockMove.y)).toBe(false);
    });

    test("should return false when provided with undefined y coord", () => {
        const mockMove = { x: 1, y: undefined };
        expect(board.isMoveInBounds(mockMove.x, mockMove.y)).toBe(false);
    });

    test("should return false when provided with undefined x coord", () => {
        const mockMove = { x: undefined, y: 2 };
        expect(board.isMoveInBounds(mockMove.x, mockMove.y)).toBe(false);
    });

    test("should return false when provided with undefined x AND y", () => {
        const mockMove = { x: undefined, y: undefined };
        expect(board.isMoveInBounds(mockMove.x, mockMove.y)).toBe(false);
    });
});
