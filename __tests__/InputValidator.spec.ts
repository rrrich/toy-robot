import { InputValidator } from "../models/InputValidator";

describe("isValidCommand", () => {
    const mp = new InputValidator();

    test("should return true when provided 'MOVE' as command", () => {
        expect(mp.isValidCommand("MOVE")).toBe(true);
    });

    test("should return false when provided 'FOO' as command", () => {
        expect(mp.isValidCommand("FOO")).toBe(false);
    });

    test("should return false when provided 'move' as command", () => {
        expect(mp.isValidCommand("move")).toBe(false);
    });

    test("should return false when provided undefined as command", () => {
        expect(mp.isValidCommand(undefined)).toBe(false);
    });

    test("should return false when provided '' as command", () => {
        expect(mp.isValidCommand("")).toBe(false);
    });
});
