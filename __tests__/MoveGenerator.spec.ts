import { Move } from "../models/Move";
import { MoveGenerator } from "../models/MoveGenerator";

describe("generateMove", () => {

    test("should return an instance of Move", () => {
        const mg = new MoveGenerator();
        const move: Move = mg.generateMove(0, 1, 0);
        expect(move).toBeInstanceOf(Move);
    });

    test("should return an instance of Move with correct xPos", () => {
        const mg = new MoveGenerator();
        const move: Move = mg.generateMove(110, 1, 0);
        expect(move.xPos).toBe(110);
    });

    test("should return an instance of Move with correct yPos", () => {
        const mg = new MoveGenerator();
        const move: Move = mg.generateMove(110, 111, 0);
        expect(move.yPos).toBe(111);
    });

    test("should return an instance of Move with correct directionDegrees", () => {
        const mg = new MoveGenerator();
        const move: Move = mg.generateMove(110, 111, 90);
        expect(move.directionDegrees).toBe(90);
    });
});

describe("generateShiftMove", () => {

    test("should return an instance of Move", () => {
        const mg = new MoveGenerator();
        const coords = { x: 0, y: 1 };
        const move: Move = mg.generateShiftMove(coords, 1, 0);
        expect(move).toBeInstanceOf(Move);
    });

    test("should return an instance of Move with correct xPos", () => {
        const mg = new MoveGenerator();
        const coords = { x: 99, y: 1 };
        const move: Move = mg.generateShiftMove(coords, 1, 90);
        expect(move.xPos).toBe(100);
    });

    test("should return an instance of Move with correct xPos", () => {
        const mg = new MoveGenerator();
        const coords = { x: 99, y: 1 };
        const move: Move = mg.generateShiftMove(coords, 1, 270);
        expect(move.xPos).toBe(98);
    });

    test("should return an instance of Move with correct yPos", () => {
        const mg = new MoveGenerator();
        const coords = { x: 99, y: 119 };
        const move: Move = mg.generateShiftMove(coords, 1, 0);
        expect(move.yPos).toBe(120);
    });

    test("should return an instance of Move with correct yPos", () => {
        const mg = new MoveGenerator();
        const coords = { x: 99, y: 119 };
        const move: Move = mg.generateShiftMove(coords, 1, 180);
        expect(move.yPos).toBe(118);
    });

    test("should return an instance of Move with correct directionDegrees", () => {
        const mg = new MoveGenerator();
        const move: Move = mg.generateMove(110, 111, 90);
        expect(move.directionDegrees).toBe(90);
    });
});
